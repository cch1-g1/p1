//===--------------------------------- test/HCS_Performance/1.8.cpp - ustchcs-checker -- --------------------------===//
//
// This file is copy-right protected.
// Copyright (c) USTCHCS.com
//
//===--------------------------------------------------------------------------------------------------------------===//
// EXTRA_ARGS="-extra-arg=-std=c++14"
class Test {
public:
  void doTest(int x, int y);

  inline int getSum(int x, int y, int z) { return x + y + z; }
  void bad(int x, int y, int z);
  void good(int x, int y, int z);
  inline void inlineMethod();

private:
  void doOtherthing(int x, int y, int z);
};

inline void Test::inlineMethod() {}

void Test::bad(int x, int y, int z) {

  while (x < y) {
    doOtherthing(x, y, z);
  }
}

void Test::good(int x, int y, int z) {
  while (x < y) {
    getSum(x, y, z); // inline method
    inlineMethod();  // inline method
  }
}

inline int getSum(int x, int y) { return x + y; }

int getValue(int x) { return x; }

void good(Test t) {
  int data[10];
  for (int &a : data) {
    ++a;
    t.inlineMethod(); // inline
  }
  int value = t.getSum(data[0], data[1], data[2]); // inline
  int value2 = getSum(data[3], data[4]);           // inline
  t.good(data[0], data[1], data[2]);               // 1
  t.bad(data[0], data[1], data[2]);                // 2
  t.inlineMethod();                                // inline
  t.good(data[0], data[1], data[2]);               // 3
  t.bad(data[0], data[1], data[2]);                // 4
  getValue(data[8]);                               // 5
  return;
}

void bad(Test t) { // CHECK-TEXT:0:1: [hcs-performance-1.8]

  int data[10];
  for (int &a : data) {
    ++a;
    t.inlineMethod(); // inline
  }
  int value = t.getSum(data[0], data[1], data[2]); // inline
  int value2 = getSum(data[3], data[4]);           // inline
  t.good(data[0], data[1], data[2]);               // 1
  t.bad(data[0], data[1], data[2]);                // 2
  t.inlineMethod();                                // inline
  t.good(data[0], data[1], data[2]);               // 3
  t.bad(data[0], data[1], data[2]);                // 4
  getValue(data[8]);                               // 5
  good(t);                                         // extra

  return;
}

// #2662
#include <map>
#include <vector>

class ATest {
public:
  ATest(int a) {}
};

class VarDecl;
class DeclRefExpr;
using VariableMap = std::map<const VarDecl *, std::vector<const DeclRefExpr *>>;

void variableInTernaryExprMerge(const VariableMap &mergeFrom,
                                VariableMap &mergeTo) {
  for (auto &decl2RefExprs : mergeFrom) {
    if (decl2RefExprs.second.size() <= 1U &&
        mergeTo.find(decl2RefExprs.first) != mergeTo.end()) {
      continue;
    }

    for (auto *expr : decl2RefExprs.second) {
      mergeTo[decl2RefExprs.first].push_back(expr);
    }
  }

  ATest a(1);

  return;
}
