//===----------------------------- test/hcs-performance/1.5.cpp - ustchcs-checker -- ------------------------------===//
//
// This file is copy-right protected.
// Copyright (c) USTCHCS.com
//
//===--------------------------------------------------------------------------------------------------------------===//
// EXTRA_ARGS="-extra-arg=-std=c++14"
void test_p(int x) {
  int sum;
  sum++;
}

void static_p(void (*pramFunc)(int i)) { // CHECK-TEXT:0:22:0:30: [hcs-performance-1.5]
  pramFunc = test_p;
}

void static_q(int i, void (*pramFunc)(int i)) { // CHECK-TEXT:0:29:0:37: [hcs-performance-1.5]
  pramFunc = test_p;
}

void static_o(int(pramFunc2)(void),      // CHECK-TEXT:0:19:0:28: [hcs-performance-1.5]
              void (*pramFunc)(int i)) { // CHECK-TEXT:0:22:0:30: [hcs-performance-1.5]
  pramFunc = test_p;
}

void test(int(pramFunc)(void)) {} // CHECK-TEXT:0:15:0:29: [hcs-performance-1.5]

void test_p(int (&pramFunc)(void)) {}
