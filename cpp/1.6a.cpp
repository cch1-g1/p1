//===--------------------------------- test/HCS_Performance/1.6a.cpp - ustchcs-checker -- -------------------------===//
//
// This file is copy-right protected.
// Copyright (c) USTCHCS.com
//
//===--------------------------------------------------------------------------------------------------------------===//
// EXTRA_ARGS="-extra-arg=-std=c++14"
// CONFIG="{CheckOptions: [{key: hcs-performance-1.6.ParameterThreshold, value: 2}]}"

class TestClass {
public:
  void goo(int a, int b, float c, double d); // declaration is ok
private:
  void f(int a, int b, ...) {}

  void doTest(int a, int b, int c, float d) { // CHECK-TEXT:0:8:0:13: [hcs-performance-1.6]
  }
};

template <typename T>
T goo(T a, T b, int c, int d) { // CHECK-TEXT:0:3:0:5: [hcs-performance-1.6]
  c = a + b;
  if (c > 0) {
    int d;

  } else {
    int f, g;
  }
  return a;
}

void hoo(int a, int b, int c) { // CHECK-TEXT:0:6:0:8: [hcs-performance-1.6]
  int d;
  int i = goo<int>(a, b, c, d);
  int j, k, l;
  int g = j > k ? 0 : 1;
}
