//===--------------------------------- test/HCS_Performance/1.7b.cpp - ustchcs-checker -- -------------------------===//
//
// This file is copy-right protected.
// Copyright (c) USTCHCS.com
//
//===--------------------------------------------------------------------------------------------------------------===//
// EXTRA_ARGS="-extra-arg=-std=c++14"
// CONFIG="{CheckOptions: [{key: hcs-performance-1.7.BranchThreshold, value: 6}, {key: hcs-performance-1.7.MaxTraverseDepth, value: 1}]}"
class Test {
public:
  void doTest(int x, int y);

  virtual void bad(int x, int y, int z) { // CHECK-TEXT:0:16:0:16: [hcs-performance-1.7]
                                          // CHECK-NOTE:-1:16:-1:16: [7 complex statements. (Threshold: 6)]

    if (x == y) {
    }
    while (x < z) {
      x++;
    }
    for (int i = 0; i < z; i++) {
    }
    switch (x) {
    case 0:
      if (x > y) {
      }
      break;
    default:
      break;
    }
    if (z < 0) { // extra
    }
    if (y < 0) {
    }
    while (z < y) {
      z++;
    }
  }

private:
  void bad2(int x, int y, int z);

  void good(int x, int y, int z) {
    if (x == y) {
    } else if (x == z) {
    } else if (y == z) {
      z = x + y;
    }
    do {
      x++;
    } while (x < z);
    for (int i = 0; i < y; i++) {
    }
    return;
  }
};

void Test::bad2(int x, int y, int z) { // CHECK-TEXT:0:12:0:12: [hcs-performance-1.7]
                                       // CHECK-NOTE:-1:12:-1:12: [7 complex statements. (Threshold: 6)]
  int data[10];
  for (int &a : data) {
    ++a;
  }
  try {
    data[x] = data[y];
  } catch (...) {
    throw;
  }
  while (data[x] < data[y]) {
    data[x]++;
  }
  if (z < x + y) {
    z++;
  } else if (z > x + y) {
    z--;
  } else if (z != 0) { // extra
  }
  if (x > y) {
  }
  if (y < 0) {
  }
  if (z > y) {
  }
}

void good(int x, int y, int z) {
  int data[10];
  for (int &a : data) {
    ++a;
  }
  try {
    data[x] = data[y];
  } catch (...) {
    throw;
  }
  while (data[x] < data[y]) {
    data[x]++;
  }
  if (z < x + y) {
    z++;
  } else if (z > x + y) {
    z--;
  } else {
  }
}
