//===--------------------------------- test/HCS_Performance/1.6b.cpp - ustchcs-checker -- -------------------------===//
//
// This file is copy-right protected.
// Copyright (c) USTCHCS.com
//
//===--------------------------------------------------------------------------------------------------------------===//
// EXTRA_ARGS="-extra-arg=-std=c++14"
// CONFIG="{CheckOptions: [{key: hcs-performance-1.6.VirtualMethodExcluded, value: 0},{key: hcs-performance-1.6.OverrideMethodExcluded, value: 1},{key: hcs-performance-1.6.PrivateMethodExcluded, value: 1}]}"
class Base {
public:
  virtual void badOverride(int x, int y, int z, int bad) = 0;
};

class Test : public Base {
public:
  void bad(int x, int y, int z, int bad);

  virtual void badV(int x, int y, int z, int bad) { // CHECK-TEXT:0:16:0:19: [hcs-performance-1.6]
  }
  virtual void badOverride(int x, int y, int z, int bad) override {
  }

private:
  void bad2(int x, int y, int z, int bad) { // OK
  }
};

void Test::bad(int x, int y, int z, int bad) { // CHECK-TEXT:0:12:0:14: [hcs-performance-1.6]
}

void good(int x, int y, int z) {
}

void bad(int x, int y, int z, int bad) { // CHECK-TEXT:0:6:0:8: [hcs-performance-1.6]
}

void good2() {
}

void good3(int x, char *y) {
}
